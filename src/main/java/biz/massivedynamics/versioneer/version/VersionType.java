/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version;

/**
 * A collection of enumerations that define different {@link VersionType}s
 * 
 * @author Cruz Julian Bishop
 * @since 0.8.0.0
 */
public enum VersionType {

    /**
     * A snapshot, commonly referred to as a nightly build
     * <br /><br />
     * These builds are usually unstable and may not actually build/compile. Be warned!
     * 
     * @since 0.8.0.0
     */
    SNAPSHOT,
    
    /**
     * An alpha version, commonly used for unstable or incomplete releases
     * <br /><br />
     * An alpha version should at least have some working features, and be able to run
     * 
     * @since 0.8.0.0
     */
    ALPHA,
    
    /**
     * A beta version, commonly used for mostly stable releases with some features missing
     * <br /><br />
     * Beta versions do contain bugs, but they are generally safe to use
     * 
     * @since 0.8.0.0
     */
    BETA,
    
    /**
     * A release candidate, commonly used to find and fix any lasting bugs
     * <br /><br />
     * Release candidates are usually stable, and are released for testing.
     * <br />
     * Think of it as quality assurance!
     * 
     * @since 1.3.0.0
     */
    RELEASE_CANDIDATE,
    
    /**
     * A stable release, also known as "Gold"
     * <br /><br />
     * There's nothing else to say. It's stable, it should work.
     * 
     * @since 0.8.0.0
     */
    STABLE;
    
    /**
     * Gets the postfix for this {@link VersionType}
     * 
     * @return The postfix
     * @since 0.8.0.0
     */
    public String getPostfix() {
        if (this != STABLE) {
            return this == RELEASE_CANDIDATE ? "-RC" : ("-" + this.name().toUpperCase());
        }
        return "";
    }
    
}
