/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version.impl;

import biz.massivedynamics.versioneer.version.Version;
import biz.massivedynamics.versioneer.version.VersionType;

/**
 * A {@link FourPartVersion} consists of four version numbers:
 * <br /><br />
 * <ol>
 *   <li>Major</li>
 *   <li>Minor</li>
 *   <li>Build</li>
 *   <li>Revision</li>
 * </ol>
 * 
 * @author Cruz Julian Bishop
 * @since 1.3.0.0
 */
public final class FourPartVersion extends Version {
    
    /**
     * Creates a new {@link FourPartVersion}
     * 
     * @param major The major version number to use
     * @since 1.3.0.0
     */
    public FourPartVersion(int major) {
        this(major, 0);
    }
    
    /**
     * Creates a new {@link FourPartVersion}
     * 
     * @param major The major version number to use
     * @param minor The minor version number to use
     * @since 1.3.0.0
     */
    public FourPartVersion(int major, int minor) {
        this(major, minor, 0);
    }
    
    /**
     * Creates a new {@link FourPartVersion}
     * 
     * @param major The major version number to use
     * @param minor The minor version number to use
     * @param build The build number to use
     * @since 1.3.0.0
     */
    public FourPartVersion(int major, int minor, int build) {
        this(major, minor, build, 0);
    }
    
    /**
     * Creates a new {@link FourPartVersion}
     * 
     * @param major The major version number to use
     * @param minor The minor version number to use
     * @param build The build number to use
     * @param revision The revision number to use
     * @since 1.3.0.0
     */
    public FourPartVersion(int major, int minor, int build, int revision) {
        this(major, minor, build, revision, VersionType.STABLE);
    }
    
    /**
     * Creates a new {@link FourPartVersion}
     * 
     * @param major The major version number to use
     * @param minor The minor version number to use
     * @param build The build number to use
     * @param revision The revision number to use
     * @param type The {@link VersionType} to use
     * @since 1.3.0.0
     */
    public FourPartVersion(int major, int minor, int build, int revision, VersionType type) {
        this(major, minor, build, revision, type, "");
    }
    
    /**
     * Creates a new {@link FourPartVersion}
     * 
     * @param major The major version number to use
     * @param minor The minor version number to use
     * @param build The build number to use
     * @param revision The revision number to use
     * @param type The {@link VersionType} to use
     * @param codeName The code name to use
     * @since 1.3.0.0
     */
    public FourPartVersion(int major, int minor, int build, int revision, VersionType type, String codeName) {
        super(type, codeName);
        this.setMajor(major);
        this.setMinor(minor);
        this.setBuild(build);
        this.setRevision(revision);
    }
    
    /**
     * The major version number
     * 
     * @since 1.3.0.0
     */
    private int major;
    
    /**
     * Gets the major version number
     * 
     * @return The major version number
     * @since 1.3.0.0
     */
    public final int getMajor() {
        return this.major;
    }
    
    /**
     * Sets the major version number
     * 
     * @param major The major version number
     * @since 1.3.0.0
     */
    public final void setMajor(int major) {
        if (major < 0) {
            major = 0;
        }
        this.major = major;
    }
    
    /**
     * The minor version number
     * 
     * @since 1.3.0.0
     */
    private int minor;
    
    /**
     * Gets the minor version number
     * 
     * @return The minor version number
     * @since 1.3.0.0
     */
    public final int getMinor() {
        return this.minor;
    }
    
    /**
     * Sets the minor version number
     * 
     * @param minor The minor version number
     * @since 1.3.0.0
     */
    public final void setMinor(int minor) {
        if (minor < 0) {
            minor = 0;
        }
        this.minor = minor;
    }
    
    /**
     * The build number
     * 
     * @since 1.3.0.0
     */
    private int build;
    
    /**
     * Gets the build number
     * 
     * @return The build number
     * @since 1.3.0.0
     */
    public final int getBuild() {
        return this.build;
    }
    
    /**
     * Sets the build number
     * 
     * @param build The build number
     * @since 1.3.0.0
     */
    public final void setBuild(int build) {
        if (build < 0) {
            build = 0;
        }
        this.build = build;
    }
    
    /**
     * The revision number
     * 
     * @since 1.3.0.0
     */
    private int revision;
    
    /**
     * Gets the revision number
     * 
     * @return The revision number
     * @since 1.3.0.0
     */
    public final int getRevision() {
        return this.revision;
    }
    
    /**
     * Sets the revision number
     * 
     * @param revision The revision number
     * @since 1.3.0.0
     */
    public final void setRevision(int revision) {
        if (revision < 0) {
            revision = 0;
        }
        this.revision = revision;
    }

    /**
     * Compares this {@link FourPartVersion} to another {@link Version}
     * 
     * @param t The other {@link Version}
     * @return The results of the comparison
     * @since 1.3.0.0
     */
    @Override
    public int compareTo(Version t) {
        
        //Make sure we're comparing FourPartVersions
        FourPartVersion other = t.toFourPartVersion();
        
        if (other.getMajor() > this.getMajor()) {
            return -1;
        } else if (other.getMajor() < this.getMajor()) {
            return 1;
        }
        
        if (other.getMinor() > this.getMinor()) {
            return -1;
        } else if (other.getMinor() < this.getMinor()) {
            return 1;
        }
        
        if (other.getBuild() > this.getBuild()) {
            return -1;
        } else if (other.getBuild() < this.getBuild()) {
            return 1;
        }
        
        if (other.getRevision() > this.getRevision()) {
            return -1;
        } else if (other.getRevision() < this.getRevision()) {
            return 1;
        }
        
        //Could not find any differences. Compare the types and return
        return this.getType().compareTo(other.getType());
    }

    /**
     * Converts this {@link FourPartVersion} to a {@link FourPartVersion}
     * 
     * @return The same {@link FourPartVersion} that you started out with.
     * @since 1.3.0.0
     */
    @Override
    public FourPartVersion toFourPartVersion() {
        return this;
    }

    /**
     * Converts this {@link FourPartVersion} to a {@link String}
     * 
     * @return A {@link String}
     * @since 1.3.0.0
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder().append(this.major).append('.')
                .append(this.minor).append('.')
                .append(this.build).append('.')
                .append(this.revision).append(this.getType().getPostfix());
        
        if (!this.getCodeName().isEmpty()) {
            builder.append(" '").append(this.getCodeName()).append("'");
        }
        
        return builder.toString();
    }

    /**
     * Clones this {@link FourPartVersion}
     * 
     * @return The cloned {@link FourPartVersion}
     * @since 1.3.0.0
     */
    @Override
    public FourPartVersion clone() {
        return new FourPartVersion(this.getMajor(), this.getMinor(),
                this.getBuild(), this.getRevision(), this.getType(), 
                this.getCodeName());
    }

    /**
     * Gets this {@link FourPartVersion}'s hashCode
     * 
     * @return The hashCode
     * @since 1.3.0.0
     */
    @Override
    public int hashCode() {
        return (this.getMajor() << 16 
                + this.getMinor() << 12 
                + this.getBuild() << 8 
                + this.getRevision() << 4 
                + this.getType().hashCode());
    }
    
}
