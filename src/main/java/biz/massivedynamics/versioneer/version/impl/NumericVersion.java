/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version.impl;

import biz.massivedynamics.versioneer.version.Version;
import biz.massivedynamics.versioneer.version.VersionType;

/**
 * A {@link NumericVersion} uses a single integer to track the version number
 *
 * @author Cruz Julian Bishop
 * @since 1.0.0.0
 */
public final class NumericVersion extends Version {
    
    /**
     * Creates a new {@link NumericVersion}
     * 
     * @param version The version number to use
     * @since 1.0.0.0
     */
    public NumericVersion(int version) {
        this(version, VersionType.STABLE);
    }
    
    /**
     * Creates a new {@link NumericVersion}
     * 
     * @param version The version number to use
     * @param type The {@link VersionType} to use
     * @since 1.0.0.0
     */
    public NumericVersion(int version, VersionType type) {
        this(version, type, "");   
    }
    
    /**
     * Creates a new {@link NumericVersion}
     * 
     * @param version The version number to use
     * @param type The {@link VersionType} to use
     * @param codeName The code name to use
     * @since 1.1.1.0
     */
    public NumericVersion(int version, VersionType type, String codeName) {
        super(type, codeName);
        this.setVersion(version);
    }
    
    /**
     * The version number to use
     * 
     * @since 1.0.0.0
     */
    private int version;
    
    /**
     * Gets the version number
     * 
     * @return The version number
     * @since 1.0.0.0
     */
    public final int getVersion() {
        return this.version;
    }
    
    /**
     * Sets the version number
     * 
     * @param version The version number
     * @since 1.0.0.0
     */
    public final void setVersion(int version) {
        if (version < 0) {
            version = 0;
        }
        this.version = version;
    }

    /**
     * Creates a {@link FourPartVersion} that represents this {@link NumericVersion}
     * 
     * @return The {@link FourPartVersion}
     * @since 1.0.0.0
     */
    @Override
    public FourPartVersion toFourPartVersion() {
        return new FourPartVersion(this.getVersion(), 0, 0, 0, this.getType(), this.getCodeName());
    }

    /**
     * Returns this {@link NumericVersion} as a {@link String}
     * 
     * @return A {@link String}
     * @since 1.0.0.0
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder().append(this.getVersion())
                .append(this.getType().getPostfix());
        
        if (!this.getCodeName().isEmpty()) {
            builder.append(" '").append(this.getCodeName()).append("'");
        }
        
        return builder.toString();
    }

    /**
     * Compares this {@link NumericVersion} with another {@link Version}
     * 
     * @param t The {@link Version}
     * @return The result
     * @since 1.0.0.0
     */
    @Override
    public int compareTo(Version t) {
        /* See if the other version is a numeric version,
         * otherwise compare them as four-part versions */
        if (!(t instanceof NumericVersion)) {
            return this.toFourPartVersion().compareTo(t.toFourPartVersion());
        }
        
        NumericVersion other = (NumericVersion) t;
        
        //Check the version number
        if (this.getVersion() > other.getVersion()) {
            return 1;
        } else if (this.getVersion() < other.getVersion()) {
            return -1;
        } else return this.getType().compareTo(other.getType());
    }
    
    /**
     * Clones this {@link NumericVersion}
     * 
     * @return The cloned {@link NumericVersion}
     * @since 1.1.0.0
     */
    @Override
    public NumericVersion clone() {
        return new NumericVersion(this.getVersion(), this.getType(), this.getCodeName());
    }
    
    /**
     * Gets this {@link NumericVersion}'s hashCode
     * 
     * @return The hashCode
     * @since 1.1.0.0
     */
    @Override
    public int hashCode() {
        return (this.getVersion() << 4 
                + this.getType().hashCode());
    }
    
}
