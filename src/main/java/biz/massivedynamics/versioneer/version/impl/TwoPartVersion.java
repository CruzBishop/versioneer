/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version.impl;

import biz.massivedynamics.versioneer.version.Version;
import biz.massivedynamics.versioneer.version.VersionType;

/**
 * A {@link TwoPartVersion} is a {@link Version} that consists of two parts:
 * <br /><br />
 * <ol>
 *   <li>Major</li>
 *   <li>Minor</li>
 * </ol>
 * 
 * @author Cruz Julian Bishop
 * @since 0.9.0.0
 */
public final class TwoPartVersion extends Version {
    
    /**
     * Creates a new {@link TwoPartVersion}
     * 
     * @param major The major version number to use
     * @since 0.9.0.0
     */
    public TwoPartVersion(int major) {
        this(major, 0);
    }
    
    /**
     * Creates a new {@link TwoPartVersion}
     * 
     * @param major The major version number to use
     * @param minor The minor version number to use
     * @since 0.9.0.0
     */
    public TwoPartVersion(int major, int minor) {
        this(major, minor, VersionType.STABLE);
    }
    
    /**
     * Creates a new {@link TwoPartVersion}
     * 
     * @param major The major version number to use
     * @param minor The minor version number to use
     * @param type The {@link TwoPartVersion} to use
     * @since 0.9.0.0
     */
    public TwoPartVersion(int major, int minor, VersionType type) {
        this(major, minor, type, "");
    }
    
    /**
     * Creates a new {@link TwoPartVersion}
     * 
     * @param major The major version number to use
     * @param minor The minor version number to use
     * @param type The {@link TwoPartVersion} to use
     * @param codeName The code name to use
     * @since 1.1.1.0
     */
    public TwoPartVersion(int major, int minor, VersionType type, String codeName) {
        super(type, codeName);
        this.setMajor(major);
        this.setMinor(minor);
    }
    
    /**
     * The major version number
     * 
     * @since 0.9.0.0
     */
    private int major;
    
    /**
     * Gets the major version number
     * 
     * @return The major version number
     * @since 0.9.0.0
     */
    public final int getMajor() {
        return this.major;
    }
    
    /**
     * Sets the major version number
     * 
     * @param major The major version number
     * @since 0.9.0.0
     */
    public final void setMajor(int major) {
        if (major < 0) {
            major = 0;
        }
        this.major = major;
    }
    
    /**
     * The minor version number
     * 
     * @since 0.9.0.0
     */
    private int minor;
    
    /**
     * Gets the minor version number
     * 
     * @return The minor version number
     * @since 0.9.0.0
     */
    public final int getMinor() {
        if (minor < 0) {
            minor = 0;
        }
        return this.minor;
    }
    
    /**
     * Sets the minor version number
     * 
     * @param minor The minor version number
     * @since 0.9.0.0
     */
    public final void setMinor(int minor) {
        this.minor = minor;
    }

    /**
     * Creates a {@link FourPartVersion} from this {@link TwoPartVersion}
     * 
     * @return The {@link FourPartVersion}
     * @since 0.9.0.0
     */
    @Override
    public FourPartVersion toFourPartVersion() {
        return new FourPartVersion(this.getMajor(), this.getMinor(),
                0, 0, this.getType(), this.getCodeName());
    }

    /**
     * Converts this {@link TwoPartVersion} to a {@link String}
     * 
     * @return A {@link String}
     * @since 0.9.0.0
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder().append(this.major).append('.')
                .append(this.minor).append(this.getType().getPostfix());
        
        if (!this.getCodeName().isEmpty()) {
            builder.append(" '").append(this.getCodeName()).append("'");
        }
        
        return builder.toString();
    }

    /**
     * Compares this {@link TwoPartVersion} to another {@link Version}
     * 
     * @param t The other {@link Version}
     * @return The comparison
     * @since 0.9.0.0
     */
    @Override
    public int compareTo(Version t) {
        //See if "t" is a two-part version
        if (!(t instanceof TwoPartVersion)) {
            //Compare as four-part versions
            return this.toFourPartVersion().compareTo(t.toFourPartVersion());
        }
        
        //We're comparing two-part versions
        TwoPartVersion other = (TwoPartVersion) t;
        
        if (other.getMajor() > this.getMajor()) {
            return -1;
        } else if (other.getMajor() < this.getMajor()) {
            return 1;
        }
        
        if (other.getMinor() > this.getMinor()) {
            return -1;
        } else if (other.getMinor() < this.getMinor()) {
            return 1;
        }
        
        //Could not find any differences. Compare the types and return
        return this.getType().compareTo(other.getType());
    }
    
    /**
     * Clones this {@link TwoPartVersion}
     * 
     * @return The cloned {@link TwoPartVersion}
     * @since 1.1.0.0
     */
    @Override
    public TwoPartVersion clone() {
        return new TwoPartVersion(this.getMajor(), this.getMinor(),
                this.getType(), this.getCodeName());
    }
    
    /**
     * Gets this {@link TwoPartVersion}'s hashCode
     * 
     * @return The hashCode
     * @since 1.1.0.0
     */
    @Override
    public int hashCode() {
        return (this.getMajor() << 8 
                + this.getMinor() << 4 
                + this.getType().hashCode());
    }
    
}
