/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version;

import biz.massivedynamics.versioneer.exceptions.InvalidVersionException;
import biz.massivedynamics.versioneer.version.impl.FourPartVersion;
import biz.massivedynamics.versioneer.version.impl.NumericVersion;
import biz.massivedynamics.versioneer.version.impl.ThreePartVersion;
import biz.massivedynamics.versioneer.version.impl.TwoPartVersion;

/**
 * An abstract class that defines the skeleton of a {@link Version}
 * <br /> <br /> 
 * All {@link Version} classes must have this class as their parent
 *
 * @author Cruz Julian Bishop
 * @since 1.2.0.0
 */
public abstract class Version implements Comparable<Version>, Cloneable {

    /**
     * Creates a new {@link Version}
     *
     * @param type The {@link VersionType} to use
     * @param codeName The code name to use
     * @since 1.2.0.0
     */
    public Version(VersionType type, String codeName) {
        this.setType(type);
        this.setCodeName(codeName);
    }
    
    /**
     * This {@link Version}'s {@link VersionType} 
     *
     * @since 1.2.0.0
     */
    private VersionType type;

    /**
     * Gets this {@link Version}'s {@link VersionType}
     *
     * @return The {@link VersionType}
     * @since 1.2.0.0
     */
    public final VersionType getType() {
        return this.type;
    }

    /**
     * Sets this {@link Version}'s {@link VersionType}
     *
     * @param type The {@link VersionType} to set
     * @since 1.2.0.0
     */
    public final void setType(VersionType type) {
        this.type = type;
    }
    
    /**
     * The code name of this {@link Version}
     *
     * @since 1.2.0.0
     */
    private String codeName;

    /**
     * Gets the code name of this {@link Version}
     *
     * @return The code name
     * @since 1.2.0.0
     */
    public final String getCodeName() {
        return this.codeName;
    }

    /**
     * Sets the code name of this {@link Version}
     *
     * @param codeName The code name
     * @since 1.2.0.0
     */
    public final void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    /**
     * Converts this {@link Version} to a {@link FourPartVersion}
     *
     * @return The {@link FourPartVersion} representing  this {@link Version}
     * @since 1.2.0.0
     */
    public abstract FourPartVersion toFourPartVersion();

    /**
     * Convert this {@link Version} to a {@link String}
     *
     * @return A {@link String}
     * @since 1.2.0.0
     */
    @Override
    public abstract String toString();

    /**
     * Compares this {@link Version} with another {@link Version} for equality
     *
     * @param obj The other object, preferably a {@link Version}!
     * @return True if equal, otherwise false
     * @since 1.2.0.0
     */
    @Override
    public final boolean equals(Object obj) {
        if (!(obj instanceof Version)) {
            return false;
        }

        //Set up the other version
        FourPartVersion version = ((Version) obj).toFourPartVersion();

        //And compare
        return ((this.compareTo(version)) == 0);
    }

    /**
     * Gets this {@link Version}'s hashcode
     *
     * @return The hashcode
     * @since 1.2.0.0
     */
    @Override
    public abstract int hashCode();

    /**
     * Clones this {@link Version}
     *
     * @return The cloned {@link Version}
     * @since 1.2.0.0
     */
    @Override
    public abstract Version clone();

    /**
     * Gets a {@link Version} from a {@link String}
     *
     * @param versionAsString The {@link Version} as a {@link String}
     * @return The {@link Version}
     * @throws InvalidVersionException The {@link Version} could not be decoded
     * @throws IllegalArgumentException The {@link VersionType} could not be decoded
     * @since 1.2.0.0
     */
    public static Version get(String versionAsString) throws InvalidVersionException, IllegalArgumentException {
        //Set up an initial (null) abstract version
        Version version = null;

        //Check to see if the string is empty
        if (versionAsString.isEmpty()) {
            throw new InvalidVersionException("Can't create a blank version!");
        }

        //Set up a split
        String[] versionParts = versionAsString.split("\\.");

        //Switch by the number of version parts
        switch (versionParts.length) {
            case 4: {
                int major = Integer.valueOf(versionParts[0]);
                int minor = Integer.valueOf(versionParts[1]);
                int build = Integer.valueOf(versionParts[2]);
                int revision;
                if (versionParts[3].contains("-") || versionParts[3].contains(" ")) {
                    revision = Integer.valueOf(versionParts[3].split("[ -]")[0]);
                } else {
                    revision = Integer.valueOf(versionParts[3]);
                }

                version = new FourPartVersion(major, minor, build, revision);
            }
            break;

            case 3: {
                int major = Integer.valueOf(versionParts[0]);
                int minor = Integer.valueOf(versionParts[1]);
                int build;
                if (versionParts[2].contains("-") || versionParts[2].contains(" ")) {
                    build = Integer.valueOf(versionParts[2].split("[ -]")[0]);
                } else {
                    build = Integer.valueOf(versionParts[2]);
                }

                version = new ThreePartVersion(major, minor, build);
            }
            break;

            case 2: {
                int major = Integer.valueOf(versionParts[0]);
                int minor;
                if (versionParts[1].contains("-") || versionParts[1].contains(" ")) {
                    minor = Integer.valueOf(versionParts[1].split("[ -]")[0]);
                } else {
                    minor = Integer.valueOf(versionParts[1]);
                }

                version = new TwoPartVersion(major, minor);
            }
            break;

            case 1: {
                int _version;
                if (versionParts[0].contains("-") || versionParts[0].contains(" ")) {
                    _version = Integer.valueOf(versionParts[0].split("[ -]")[0]);
                } else {
                    _version = Integer.valueOf(versionParts[0]);
                }

                version = new NumericVersion(_version);
            }
            break;
        }

        //Check the version before we start on the rest 
        if (version == null) {
            throw new InvalidVersionException("Version \"" + versionAsString + "\" could not be decoded");
        }
        
        //Get the last part
        String lastPart = versionParts[versionParts.length - 1];
        
        //Okay, the version numbers are all done. Let's start on types and names
        if (lastPart.contains("-")) {
            
            //We're getting the type
            String versionTypeString;
            
            //See if there's a breaking space
            if (lastPart.contains(" ")) {
                versionTypeString = lastPart.substring(lastPart.indexOf("-") + 1, lastPart.indexOf(" "));
            } else {
                versionTypeString = lastPart.substring(lastPart.indexOf("-") + 1, lastPart.length());
            }

            if (versionTypeString.equalsIgnoreCase("RC")) {
                version.setType(VersionType.RELEASE_CANDIDATE);
            } else {
                version.setType(VersionType.valueOf(versionTypeString));
            }
        }
        
        //Okay, type is done. Let's get the name!
        if (lastPart.contains(" ")) {
            
            //We're getting the code name
            if (lastPart.contains("'")) {
                version.setCodeName(lastPart.substring(lastPart.indexOf("'") + 1, lastPart.lastIndexOf("'")));
            } else if (lastPart.contains("\"")) {
                version.setCodeName(lastPart.substring(lastPart.indexOf("\"") + 1, lastPart.lastIndexOf("\"")));
            }
            
        }
        
        //Everything is done! Let's return the version
        return version;
    }

    /**
     * Checks to see if this {@link Version} is stable
     *
     * @return True if stable, otherwise false
     * @since 1.3.1.0
     */
    public boolean isStable() {
        return this.type.equals(VersionType.STABLE);
    }
}
