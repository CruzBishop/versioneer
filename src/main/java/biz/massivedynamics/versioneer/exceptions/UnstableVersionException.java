/*
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package biz.massivedynamics.versioneer.exceptions;

import biz.massivedynamics.versioneer.annotations.Untested;
import biz.massivedynamics.versioneer.version.Version;

/**
 * An {@link Exception} that is thrown when a {@link Version} is unstable
 *
 * @since 1.3.1.0
 */
@Untested
public class UnstableVersionException extends Exception {

    /**
     * The unstable {@link Version}
     *
     * @since 1.3.1.0
     */
    private Version version = null;

    /**
     * Gets the unstable {@link Version}
     *
     * @return The {@link Version}
     * @since 1.3.1.0
     */
    public Version getVersion() {
        return this.version;
    }

    /**
     * Creates a new {@link UnstableVersionException} with no specified version
     *
     * @since 1.3.1.0
     */
    public UnstableVersionException() {
        super("The specified version is unstable.");
    }

    /**
     * Creates a new {@link UnstableVersionException} with a specified unstable {@link Version}
     *
     * @param version The unstable version
     * @since 1.3.1.0
     */
    public UnstableVersionException(Version version) {
        super("The specified version is unstable.");

        this.version = version;
    }
}
