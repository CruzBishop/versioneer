/*
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package biz.massivedynamics.versioneer.exceptions;

import biz.massivedynamics.versioneer.annotations.Untested;
import biz.massivedynamics.versioneer.version.Version;

/**
 * An {@link Exception} that is thrown when a {@link Version} is too low
 *
 * @author Cruz Julian Bishop
 * @since 1.3.1.0
 */
@Untested
public class VersionTooLowException extends Exception {

    /**
     * The minimum {@link Version}
     *
     * @since 1.3.1.0
     */
    private Version minimumVersion = null;

    /**
     * The supplied {@link Version}
     *
     * @since 1.3.1.0
     */
    private Version suppliedVersion = null;

    /**
     * Gets the minimum {@link Version}
     *
     * @return The minimum {@link Version}
     * @since 1.3.1.0
     */
    public Version getMinimumVersion() {
        return this.minimumVersion;
    }

    /**
     * Gets the supplied {@link Version}
     *
     * @return The supplied {@link Version}
     * @since 1.3.1.0
     */
    public Version getSuppliedVersion() {
        return this.suppliedVersion;
    }

    /**
     * Creates a new {@link VersionTooLowException} without any data
     *
     * @since 1.3.1.0
     */
    public VersionTooLowException() {
        super("The specified version is too low.");
    }

    /**
     * Creates a new {@link VersionTooLowException} with the minimum version
     *
     * @param minimumVersion The minimum version
     * @since 1.3.1.0
     */
    public VersionTooLowException(Version minimumVersion) {
        super("The specified version is too low.");

        this.minimumVersion = minimumVersion;
    }

    /**
     * Creates a new {@link VersionTooLowException}
     *
     * @param minimumVersion The minimum version
     * @param suppliedVersion The supplied version
     * @throws VersionTooHighException The supplied version is lower than the minimum version
     * @since 1.3.1.0
     */
    public VersionTooLowException(Version minimumVersion, Version suppliedVersion) throws VersionTooHighException {
        super("The specified version is too low.");

        if (minimumVersion.compareTo(suppliedVersion) >= 0) {
            throw new VersionTooHighException(suppliedVersion);
        }

        this.minimumVersion = minimumVersion;
        this.suppliedVersion = suppliedVersion;
    }
    
}
