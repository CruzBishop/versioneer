/*
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package biz.massivedynamics.versioneer.exceptions;

import biz.massivedynamics.versioneer.annotations.Alpha;
import biz.massivedynamics.versioneer.annotations.Beta;

/**
 * An {@link Exception} that is thrown when code is marked as unstable ({@link Alpha}, {@link Beta}, etc)
 *
 * @since 1.3.1.0
 */
public class UnstableCodeException extends Exception {

    /**
     * Creates a new {@link UnstableCodeException} with no child
     *
     * @since 1.3.1.0
     */
    public UnstableCodeException() {
        super("Unstable code is being used");
    }

    /**
     * Creates a new {@link UnstableCodeException}
     *
     * @param child The child {@link Exception}
     * @since 1.3.1.0
     */
    public UnstableCodeException(Exception child) {
        super("Unstable code is being used", child);
    }
}
