/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer;

import biz.massivedynamics.versioneer.version.VersionType;
import biz.massivedynamics.versioneer.version.impl.FourPartVersion;

/**
 * A utility class for Versioneer
 * <br /><br />
 * Simply holds the version of Versioneer being used at the moment.
 *
 * @author Cruz Julian Bishop
 * @since 1.0.0.0
 */
public final class Versioneer {
    
    /**
     * A dummy constructor
     * 
     * @since 1.1.2.0
     * @throws IllegalAccessException You may not make new instances
     */
    public Versioneer() throws IllegalAccessException {
        throw new IllegalAccessException("You may not make new instances");
    }
    
    /**
     * Gets the version of Versioneer being used
     * 
     * @return The version
     * @since 1.0.0.0
     */
    public static FourPartVersion getVersion() {
        return new FourPartVersion(1, 4, 0, 0, VersionType.SNAPSHOT, "ASPIRE");
    }
    
}
