/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package biz.massivedynamics.versioneer.exceptions;

import biz.massivedynamics.versioneer.version.impl.NumericVersion;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * A test class that tests {@link VersionTooLowException}
 *
 * @author Cruz Julian Bishop
 * @since 1.3.1.0
 */
public class VersionTooLowExceptionTest {

    /**
     * Tests basic construction
     *
     * @since 1.3.1.0
     */
    @Test
    public void testBasicConstructor() {
        VersionTooLowException exception = new VersionTooLowException();

        assertEquals("The specified version is too low.", exception.getMessage());

        assertNull(exception.getMinimumVersion());
        assertNull(exception.getSuppliedVersion());
    }

    /**
     * Tests 'half' construction
     *
     * @since 1.3.1.0
     */
    @Test
    public void testHalfConstructor() {
        VersionTooLowException exception = new VersionTooLowException(new NumericVersion(15));

        assertEquals("The specified version is too low.", exception.getMessage());

        assertEquals(new NumericVersion(15), exception.getMinimumVersion());
        assertNull(exception.getSuppliedVersion());
    }

    /**
     * Tests full construction
     *
     * @since 1.3.1.0
     */
    @Test
    public void testFullConstructor() {
        try {

            VersionTooLowException exception = new VersionTooLowException(new NumericVersion(14), new NumericVersion(15));

            assertEquals("The specified version is too low.", exception.getMessage());

            assertEquals(new NumericVersion(14), exception.getMinimumVersion());
            assertEquals(new NumericVersion(15), exception.getSuppliedVersion());

        } catch (VersionTooHighException thrown) {
            fail("Unexpected exception found: " + thrown.toString());
        }

        try {
            //noinspection ThrowableInstanceNeverThrown
            new VersionTooLowException(new NumericVersion(14), new NumericVersion(13));

            fail("Expected failure.");
        } catch (VersionTooHighException thrown) {
            assertEquals(new NumericVersion(13), thrown.getMaximumVersion());
        }


    }

}
