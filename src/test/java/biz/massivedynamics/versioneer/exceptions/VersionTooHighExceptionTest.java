/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package biz.massivedynamics.versioneer.exceptions;

import biz.massivedynamics.versioneer.version.impl.NumericVersion;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * A test class that tests {@link VersionTooHighException}
 *
 * @author Cruz Julian Bishop
 * @since 1.3.1.0
 */
public class VersionTooHighExceptionTest {

    /**
     * Tests basic construction
     *
     * @since 1.3.1.0
     */
    @Test
    public void testBasicConstructor() {
        VersionTooHighException exception = new VersionTooHighException();

        assertEquals("The specified version is too high.", exception.getMessage());

        assertNull(exception.getMaximumVersion());
        assertNull(exception.getSuppliedVersion());
    }

    /**
     * Tests 'half' construction
     *
     * @since 1.3.1.0
     */
    @Test
    public void testHalfConstructor() {
        VersionTooHighException exception = new VersionTooHighException(new NumericVersion(15));

        assertEquals("The specified version is too high.", exception.getMessage());

        assertEquals(new NumericVersion(15), exception.getMaximumVersion());
        assertNull(exception.getSuppliedVersion());
    }

    /**
     * Tests full construction
     *
     * @since 1.3.1.0
     */
    @Test
    public void testFullConstructor() {
        try {

            VersionTooHighException exception = new VersionTooHighException(new NumericVersion(15), new NumericVersion(14));

            assertEquals("The specified version is too high.", exception.getMessage());

            assertEquals(new NumericVersion(15), exception.getMaximumVersion());
            assertEquals(new NumericVersion(14), exception.getSuppliedVersion());

        } catch (VersionTooLowException thrown) {
            fail("Unexpected exception found: " + thrown.toString());
        }

        try {
            //noinspection ThrowableInstanceNeverThrown
            new VersionTooHighException(new NumericVersion(15), new NumericVersion(16));

            fail("Expected failure.");
        } catch (VersionTooLowException thrown) {
            assertEquals(new NumericVersion(16), thrown.getMinimumVersion());
        }


    }

}
