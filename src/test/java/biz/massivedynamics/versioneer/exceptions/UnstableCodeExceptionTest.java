/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.exceptions;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * A test that tests {@link UnstableCodeException}
 *
 * @author Cruz Julian Bishop
 * @since 1.3.1.0
 */
public class UnstableCodeExceptionTest {
    
    /**
     * Tests the default constructor
     * 
     * @since 1.3.1.0
     */
    @Test
    public void testDefaultConstruction() {
        UnstableCodeException ex = new UnstableCodeException();

        assertEquals("Unstable code is being used", ex.getMessage());
    }
    
    /**
     * Tests creating an {@link UnstableCodeException} with a child
     * 
     * @since 1.3.1.0
     */
    @Test
    public void testWithChild() {
        try {
            throw new ArithmeticException("Nothing to do here");
        } catch (ArithmeticException e) {
            UnstableCodeException ex = new UnstableCodeException(e);

            assertEquals("java.lang.ArithmeticException", ex.getCause().getClass().getName());
        }
    }
    
}
