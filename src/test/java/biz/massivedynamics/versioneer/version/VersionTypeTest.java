/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Tests version types
 * 
 * @author Cruz Julian Bishop
 */
public class VersionTypeTest {

    /**
     * Tests the valueOf() method
     */
    @Test
    public void testValueOf() {
        assertEquals(VersionType.valueOf("SNAPSHOT"), VersionType.SNAPSHOT);
        assertEquals(VersionType.valueOf("ALPHA"), VersionType.ALPHA);
        assertEquals(VersionType.valueOf("BETA"), VersionType.BETA);
        assertEquals(VersionType.valueOf("RELEASE_CANDIDATE"), VersionType.RELEASE_CANDIDATE);
        assertEquals(VersionType.valueOf("STABLE"), VersionType.STABLE);
    }
    
    /**
     * Tests the postfix system
     */
    @Test
    public void testPostfix() {
        assertEquals(VersionType.SNAPSHOT.getPostfix(), "-SNAPSHOT");
        assertEquals(VersionType.ALPHA.getPostfix(), "-ALPHA");
        assertEquals(VersionType.BETA.getPostfix(), "-BETA");
        assertEquals(VersionType.RELEASE_CANDIDATE.getPostfix(), "-RC");
        assertEquals(VersionType.STABLE.getPostfix(), "");
    }
    
    /**
     * Tests the ordering system
     */
    @Test
    public void testOrdering() {
        assertTrue(VersionType.SNAPSHOT.compareTo(VersionType.ALPHA) == -1);
        assertTrue(VersionType.ALPHA.compareTo(VersionType.BETA) == -1);
        assertTrue(VersionType.BETA.compareTo(VersionType.RELEASE_CANDIDATE) == -1);
        assertTrue(VersionType.RELEASE_CANDIDATE.compareTo(VersionType.STABLE) == -1);
    }
    
}
