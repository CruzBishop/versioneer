/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version.impl;

import biz.massivedynamics.versioneer.version.VersionTest;
import biz.massivedynamics.versioneer.version.VersionType;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests numeric versions
 *
 * @author Cruz Julian Bishop
 */
public class NumericVersionTest extends VersionTest<NumericVersion> {
    
    /**
     * Prepares the primary version
     */
    @Override
    @Before
    public void preparePrimaryVersion() {
        this.version = new NumericVersion(1337);
    }
    
    /**
     * Tests "toString" functions
     */
    @Test
    public void testToString() {
        
        //Check the initial output
        assertEquals("1337", version.toString());
        
        //Make it a snapshot
        version.setType(VersionType.SNAPSHOT);
        
        //And check the output
        assertEquals("1337-SNAPSHOT", version.toString());
        
        //Add a code name
        version.setCodeName("Something");
        
        //And check it
        assertEquals("1337-SNAPSHOT 'Something'", version.toString());
    }
    
    /**
     * Tests version numbers
     */
    @Test
    public void testVersion() {
        
        //Check the initial version number
        assertEquals(1337, version.getVersion());
        
        //Set a new version number
        version.setVersion(2);
        
        //Check the new number
        assertEquals(2, version.getVersion());
        
        //Make the number negative
        version.setVersion(-15);
        
        //And make sure it's equal to zero
        assertEquals(0, version.getVersion());
    }

    /**
     * Tests comparisons of versions
     */
    @Test
    public void testCompareTo() {
        assertEquals(1, version.compareTo(new NumericVersion(1336)));
        assertEquals(-1, version.compareTo(new NumericVersion(1338)));
    }
    
}
