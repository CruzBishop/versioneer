/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version.impl;

import biz.massivedynamics.versioneer.version.VersionTest;
import biz.massivedynamics.versioneer.version.VersionType;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests three-part versions
 *
 * @author Cruz Julian Bishop
 */
public class ThreePartVersionTest extends VersionTest<ThreePartVersion> {
    
    /**
     * Prepares the primary version
     */
    @Override
    @Before
    public void preparePrimaryVersion() {
        this.version = new ThreePartVersion(1, 0, 0);
    }
    
    /**
     * Tests the construction of {@link ThreePartVersion}s
     * 
     * @since 1.3.0.0
     */
    @Test
    public void testConstruction() {
        ThreePartVersion one = new ThreePartVersion(1);
        
        ThreePartVersion two = new ThreePartVersion(1, 0);
        
        ThreePartVersion three = new ThreePartVersion(1, 0, 0);

        assertEquals(two, one);
        assertEquals(three, one);

        assertEquals(three, two);
    }
    
    /**
     * Tests "toString" functions
     */
    @Test
    public void testToString() {
        
        //Check the initial output
        assertEquals("1.0.0", version.toString());
        
        //Make it a snapshot
        version.setType(VersionType.SNAPSHOT);
        
        //And check the output
        assertEquals("1.0.0-SNAPSHOT", version.toString());
        
        //Add a code name
        version.setCodeName("Something");
        
        //And check it
        assertEquals("1.0.0-SNAPSHOT 'Something'", version.toString());
    }
    
    /**
     * Tests major version numbers
     */
    @Test
    public void testMajor() {
        
        //Check the initial major number
        assertEquals(1, version.getMajor());
        
        //Set a new major number
        version.setMajor(2);
        
        //Check the new number
        assertEquals(2, version.getMajor());
        
        //Make the number negative
        version.setMajor(-15);
        
        //And make sure it's equal to zero
        assertEquals(0, version.getMajor());
    }
    
    /**
     * Tests minor version numbers
     */
    @Test
    public void testMinor() {

        //Check the initial minor number
        assertEquals(0, version.getMinor());
        
        //Set a new minor number
        version.setMinor(3);
        
        //Check the new number
        assertEquals(3, version.getMinor());
        
        //Make the number negative
        version.setMinor(-15);
        
        //And make sure it's equal to zero
        assertEquals(0, version.getMinor());
    }
    
    /**
     * Tests build numbers
     */
    @Test
    public void testBuildNumber() {
        
        //Check the initial build number
        assertEquals(0, version.getBuild());
        
        //Set a new build number
        version.setBuild(4);
        
        //Check the new number
        assertEquals(4, version.getBuild());
        
        //Make the number negative
        version.setBuild(-15);
        
        //And make sure it's equal to zero
        assertEquals(0, version.getBuild());
    }
    
}
