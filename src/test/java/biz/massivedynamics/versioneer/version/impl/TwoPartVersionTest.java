/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version.impl;

import biz.massivedynamics.versioneer.version.VersionTest;
import biz.massivedynamics.versioneer.version.VersionType;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests two-part versions
 *
 * @author Cruz Julian Bishop
 */
public class TwoPartVersionTest extends VersionTest<TwoPartVersion> {
    
    /**
     * Prepares the primary version
     */
    @Override
    @Before
    public void preparePrimaryVersion() {
        this.version = new TwoPartVersion(1, 0);
    }
    
    /**
     * Tests the construction of {@link TwoPartVersion}s
     * 
     * @since 1.3.0.0
     */
    @Test
    public void testConstruction() {
        TwoPartVersion one = new TwoPartVersion(1);

        assertEquals(version, one);
    }
    
    /**
     * Tests "toString" functions
     */
    @Test
    public void testToString() {
        
        //Check the initial output
        assertEquals("1.0", version.toString());
        
        //Make it a snapshot
        version.setType(VersionType.SNAPSHOT);
        
        //And check the output
        assertEquals("1.0-SNAPSHOT", version.toString());
        
        //Add a code name
        version.setCodeName("Something");
        
        //And check it
        assertEquals("1.0-SNAPSHOT 'Something'", version.toString());
    }
    
    /**
     * Tests major version numbers
     */
    @Test
    public void testMajor() {
        
        //Check the initial major number
        assertEquals(1, version.getMajor());
        
        //Set a new major number
        version.setMajor(2);
        
        //Check the new number
        assertEquals(2, version.getMajor());
        
        //Make the number negative
        version.setMajor(-15);
        
        //And make sure it's equal to zero
        assertEquals(0, version.getMajor());
    }
    
    /**
     * Tests minor version numbers
     */
    @Test
    public void testMinor() {
        
        //Check the initial minor number
        assertEquals(0, version.getMinor());
        
        //Set a new minor number
        version.setMinor(3);
        
        //Check the new number
        assertEquals(3, version.getMinor());
        
        //Make the number negative
        version.setMinor(-15);
        
        //And make sure it's equal to zero
        assertEquals(0, version.getMinor());
    }

    /**
     * Tests comparisons of versions
     */
    @Test
    public void testCompareTo() {
        assertEquals(-1, version.compareTo(new TwoPartVersion(1, 1)));
        assertEquals(1, version.compareTo(new TwoPartVersion(0, 1)));
        assertEquals(-1, version.compareTo(new TwoPartVersion(2, 0)));
        assertEquals(1, new TwoPartVersion(1, 1).compareTo(version));
    }
    
}
