/*
 * The MIT License
 *
 * Copyright 2012 Massive Dynamics.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.massivedynamics.versioneer.version;

import biz.massivedynamics.versioneer.exceptions.InvalidVersionException;
import biz.massivedynamics.versioneer.version.impl.FourPartVersion;
import biz.massivedynamics.versioneer.version.impl.NumericVersion;
import biz.massivedynamics.versioneer.version.impl.ThreePartVersion;
import biz.massivedynamics.versioneer.version.impl.TwoPartVersion;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 * A {@link Version} test class
 * 
 * @param <T> The {@link Version} class being tested
 *
 * @author Cruz Julian Bishop
 * @since 1.3.0.0
 */
public abstract class VersionTest<T extends Version> {
    
    /**
     * The primary version being tested
     * 
     * @since 1.3.0.0
     */
    protected T version;
    
    /**
     * Prepares the primary version
     * 
     * @since 1.3.0.0
     */
    @Before
    public abstract void preparePrimaryVersion();
    
    /**
     * Tests self equality on three measures:
     * 
     * 1: Itself
     * 2: A clone
     * 3: A generic representation
     * 
     * @since 1.3.0.0
     */
    @Test
    public void testSelfEquality() {
        assertEquals(version, version);
        assertEquals(version, version.clone());
        assertEquals(version, version.toFourPartVersion());
        
        assertEquals(version.hashCode(), version.hashCode());
        assertEquals(version.hashCode(), version.clone().hashCode());
        
        assertEquals(version.compareTo(version), 0);
        assertEquals(version.compareTo(version.clone()), 0);
        assertEquals(version.compareTo(version.toFourPartVersion()), 0);
    }
    
    /**
     * Tests exceptions in equality
     * 
     * @since 1.3.0.0
     */
    @Test
    @SuppressWarnings("AssertEqualsBetweenInconvertibleTypes")
    public void testEqualityException() {
        //noinspection EqualsBetweenInconvertibleTypes
        assertFalse(version.equals("Hello"));
    }
    
    /**
     * Tests version type comparisons
     * 
     * @since 1.3.0.0
     */
    @Test
    public void testVersionType() {
        assertEquals(version.getType(), VersionType.STABLE);
        
        version.setType(VersionType.BETA);
        
        Version anotherVersion = version.clone();
        anotherVersion.setType(VersionType.ALPHA);
        
        assertFalse(version.getType().equals(anotherVersion.getType()));
        assertFalse(version.equals(anotherVersion));
    }
    
    /**
     * Tests the stability of {@link Version}s
     * 
     * @since 1.3.1.0
     */
    @Test
    public void testStability() {
        assertTrue(version.isStable());
        
        for (VersionType type : VersionType.values()) {
            if (!type.equals(VersionType.STABLE)) {
                version.setType(type);
                assertFalse(version.isStable());
            }
        }
    }
    
    /**
     * Tests code name comparisons
     * 
     * @since 1.3.0.0
     */
    @Test
    public void testCodeName() {
        assertEquals(version.getCodeName(), "");
        
        Version anotherVersion = version.clone();
        
        version.setCodeName("Original");
        anotherVersion.setCodeName("Fake");
        
        assertEquals(version.getCodeName(), "Original");
        assertFalse(version.getCodeName().equals(anotherVersion.getCodeName()));
    }
    
    /**
     * Ensures that {@link Version#get(java.lang.String)} raises an
     * exception when it is blank.
     * 
     * @throws InvalidVersionException Blank versions are not allowed.
     */
    @Test(expected=InvalidVersionException.class)
    public void testBlankVersionGet() throws InvalidVersionException {
        Version.get("");
    }
    
    /**
     * Tests the {@link Version} types given by {@link Version#get(java.lang.String)}
     * 
     * @throws InvalidVersionException Blank versions are not allowed
     */
    @Test
    public void testVersionGet() throws InvalidVersionException {
        assertTrue(Version.get("1") instanceof NumericVersion);
        assertTrue(Version.get("1.1") instanceof TwoPartVersion);
        assertTrue(Version.get("1.1.1") instanceof ThreePartVersion);
        assertTrue(Version.get("1.1.1.1") instanceof FourPartVersion);
        
        assertTrue(Version.get("1-BETA").getType().equals(VersionType.BETA));
        assertTrue(Version.get("1.1-ALPHA").getType().equals(VersionType.ALPHA));
        assertTrue(Version.get("1.1.1-RC").getType().equals(VersionType.RELEASE_CANDIDATE));
        assertTrue(Version.get("1.1.1.1-SNAPSHOT").getType().equals(VersionType.SNAPSHOT));
        
        assertTrue(Version.get("1 'Yes'").getCodeName().equals("Yes"));
        assertTrue(Version.get("1.1 \"No\"").getCodeName().equals("No"));
        assertTrue(Version.get("1.1.1 'Maybe'").getCodeName().equals("Maybe"));
        assertTrue(Version.get("1.1.1.1 'Derp'").getCodeName().equals("Derp"));
        
        assertTrue(Version.get("1-SNAPSHOT 'Yes'") instanceof NumericVersion);
        assertTrue(Version.get("1.1-BETA \"No\"") instanceof TwoPartVersion);
        assertTrue(Version.get("1.1.1-ALPHA 'Maybe'") instanceof ThreePartVersion);
        assertTrue(Version.get("1.1.1.1-ALPHA 'Derp'") instanceof FourPartVersion);
    }
    
    /**
     * Ensures that five-part versions (and above) cannot be made
     * 
     * @throws InvalidVersionException Cannot decode
     */
    @Test(expected=InvalidVersionException.class)
    public void testFivePartVersion() throws InvalidVersionException {
        Version.get("1.0.0.0.0-BETA 'Lovelace'");
    }
    
    /**
     * Ensures that zero-part versions cannot be made
     * 
     * @throws InvalidVersionException Cannot decode
     */
    @Test(expected=NumberFormatException.class)
    public void testZeroPartVersion() throws InvalidVersionException {
        Version.get("-BETA 'Lovelace'");
    }
    
}
